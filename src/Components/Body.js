import React from 'react'
import './All.css';
import Logo from '../Image/logo_black.png';

const Body = () => {
    return (
        <>
            <div className='Container'>
                <span className='info-side'>
                    <h1>Hi NOOB HERE!</h1>
                    <p>The noob (Persian: اشرفی ) was a gold coin issued by Muslim dynasties in the Middle East, Central Asia and South Asia. It was worth two mohurs.</p>
                </span>
                <span className='image-side'>
                    <img src={Logo} alt="logo" width="500px" height="450px" />
                </span>
            </div>
        </>
    )
}

export default Body