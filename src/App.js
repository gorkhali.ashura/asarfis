import React from 'react'
import Header from './Components/Header';
import Body from './Components/Body';
import Footer from './Components/Footer'
import Profile from './Components/Profile'

const App = () => {
  return (
    <>
    <Header />
    <Body />
    <Footer />
    <Profile />
    </>
  )
}

export default App