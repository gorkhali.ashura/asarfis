import React, { useState } from 'react'
import Pic from '../Image/pic.jpg';
import './All.css';


const Profile = () => {

  return (
    <>

        <div className='profile-container'>
      <div className='profile'>
        <h1>Hi! I am Anil Bahik Thapa.</h1>
        <img src={Pic} alt='' width="300px" height="200px" />
      </div>
      <hr />
      <p className='paragraph'>

        An About Me page is one of the most important parts of your portfolio, website, or blog. This page is where prospective employers, potential clients, website users, and other professional and personal connections go to learn about who you are and what you do. And it's an ideal resource for promoting your professional brand.

        It can be challenging to write about yourself. However, the good news is if you follow the formula and tips below, you should be able to generate an engaging ‘About Me’ statement without too much of a struggle. </p>
        </div>

    
    </>
  )
}

export default Profile;