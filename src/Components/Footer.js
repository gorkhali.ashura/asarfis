import React from 'react'
import './All.css';

const Footer = () => {
  return (
    <>
    <div className='footer'>
        <p>
            @Copyright : 2022 By React Team
        </p>
    </div>
    </>
  )
}

export default Footer